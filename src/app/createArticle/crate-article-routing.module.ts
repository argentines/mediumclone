import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'
import {CreateArticleComponent} from 'src/app/createArticle/components/create-article/create-article.component'

const routes: Routes = [
  {path: 'article/new', component: CreateArticleComponent},
  //{path: 'login', component: LoginComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrateArticleRoutingModule {}
