import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {EffectsModule} from '@ngrx/effects'
import {StoreModule} from '@ngrx/store'
import {reducers} from 'src/app/createArticle/store/reducers'

import {CrateArticleRoutingModule} from 'src/app/createArticle/crate-article-routing.module'
import {ArticleFormModule} from 'src/app/shared/modules/articleForm/article-form.module'

import {CreateArticleComponent} from 'src/app/createArticle/components/create-article/create-article.component'

import {CreateArticleService} from 'src/app/createArticle/services/create-article.service'

import {CreateArticleEffect} from 'src/app/createArticle/store/effects/createArticle.effect'
import {BackendErrorsMessageModule} from 'src/app/shared/modules/backendErrorMessages/backendErrorsMessage.module'


@NgModule({
  imports: [CommonModule,
    CrateArticleRoutingModule,
    ArticleFormModule,
    EffectsModule.forFeature([CreateArticleEffect]),
    StoreModule.forFeature('createArticle', reducers), BackendErrorsMessageModule],
  providers: [CreateArticleService],
  declarations: [CreateArticleComponent]
})

export class CrateArticleModule{}
