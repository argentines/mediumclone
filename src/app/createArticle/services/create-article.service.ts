import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'

import {ArticleInputInterface} from 'src/app/shared/types/articleInput.interface'
import {SaveArticleResponseInterface} from 'src/app/shared/types/saveArticleResponse.interface'

import {environment} from 'src/environments/environment'

@Injectable()
export class CreateArticleService{

  constructor(private http: HttpClient) {}

  createArticle(articleInput: ArticleInputInterface): Observable<ArticleInputInterface> {
    const fullUrl = `${environment.apiUrl}/articles`
    return this.http.post<SaveArticleResponseInterface>(fullUrl, articleInput).pipe(
      map((response: SaveArticleResponseInterface) => response.article)
    )
  }
}
