import {BrowserModule} from '@angular/platform-browser'
import {NgModule} from '@angular/core'
import {StoreModule} from '@ngrx/store'
import {EffectsModule} from '@ngrx/effects'
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http'
import {StoreRouterConnectingModule, routerReducer} from '@ngrx/router-store'

import {AuthModule} from 'src/app/auth/auth.module'
import {TopBarModule} from 'src/app/shared/modules/topBar/topBar.module'
import {AppRoutingModule} from 'src/app/app-routing.module'
import {StoreDevtoolsModule} from '@ngrx/store-devtools'
import {GlobalFeedModule} from 'src/app/globalFeed/global-feed.module'
import {YourFeedModule} from 'src/app/yourFeed/your-feed.module'
import {TagFeedModule} from 'src/app/tagFeed/tag-feed.module'
import {ArticleModule} from 'src/app/article/article.module'
import {CrateArticleModule} from 'src/app/createArticle/crate-article.module'
import {PersistenceService} from 'src/app/shared/services/persistence.service'
import {AuthInterceptor} from 'src/app/shared/services/authinterceptor.service'
import {EditArticleModule} from 'src/app/editArticle/edit-article.module'
import {SettingsModule} from 'src/app/settings/settings.module'
import {UserProfileModule} from 'src/app/userProfile/user-profile.module'

import {AppComponent} from 'src/app/app.component'
import {environment} from 'src/environments/environment'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AuthModule,
    AppRoutingModule,
    HttpClientModule,
    TopBarModule,
    GlobalFeedModule,
    YourFeedModule,
    TagFeedModule,
    CrateArticleModule,
    ArticleModule,
    EditArticleModule,
    SettingsModule,
    UserProfileModule,
    StoreRouterConnectingModule.forRoot(),
    StoreModule.forRoot({router: routerReducer}, {}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [
    PersistenceService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
