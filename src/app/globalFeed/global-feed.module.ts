import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {GlobalFeedComponent} from 'src/app/globalFeed/components/global-feed/global-feed.component'

import {GlobalFeedRoutingModule} from 'src/app/globalFeed/global-feed-routing.module'
import {FeedModule} from 'src/app/shared/modules/feed/feed.module'
import {BannerModule} from 'src/app/shared/modules/banner/banner.module'
import {PopularTagsModule} from 'src/app/shared/modules/popularTags/popular-tags.module'
import {FeedTogglerModule} from 'src/app/shared/modules/feedToggler/feed-toggler.module'

@NgModule({
    imports: [CommonModule,
              GlobalFeedRoutingModule,
              FeedModule,
              BannerModule,
              PopularTagsModule,
              FeedTogglerModule],
  declarations: [GlobalFeedComponent]
})
export class GlobalFeedModule {}
