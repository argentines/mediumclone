import {Injectable} from '@angular/core'
import {Actions, createEffect, ofType} from '@ngrx/effects'
import {catchError, map, switchMap} from 'rxjs/operators'
import {of} from 'rxjs'

import {getUserProfileAction, getUserProfileFailureAction, getUserProfileSuccessAction} from 'src/app/userProfile/store/actions/getUserProfile.action'
import {UserProfileService} from 'src/app/userProfile/services/user-profile.service'
import {ProfileInterface} from 'src/app/shared/types/profile.interface'

@Injectable()
export class GetUserProfileEffect {

  getUserProfile$ = createEffect(() => {
      return this.actions$.pipe(
          ofType(getUserProfileAction),
          switchMap(({slug}) => {
            return this.userProfileService.getUserProfile(slug).pipe(
              map((userProfile: ProfileInterface) => {
                return getUserProfileSuccessAction({userProfile});
              }),
              catchError(() => {
                return of(getUserProfileFailureAction());
              })
            );
          })
      );
  });

  constructor(private actions$: Actions,
              private userProfileService: UserProfileService) {}
}
