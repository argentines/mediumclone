import {Action, createReducer, on} from '@ngrx/store'

import {UserProfileStateInterface} from '../types/UserProfileState.Interface'
import {getUserProfileAction, getUserProfileFailureAction, getUserProfileSuccessAction} from 'src/app/userProfile/store/actions/getUserProfile.action'

const initialState: UserProfileStateInterface = {
  isLoading: false,
  error: null,
  data: null
}

const userProfileReducer = createReducer(
  initialState,
  on(
    getUserProfileAction,
    (state): UserProfileStateInterface => ({
      ...state,
      isLoading: true
    })
  ),
  on(
    getUserProfileSuccessAction,
    (state, action): UserProfileStateInterface => ({
      ...state,
      isLoading: false,
      data: action.userProfile
    })
  ),
  on(
    getUserProfileFailureAction,
    (state): UserProfileStateInterface => ({
      ...state,
      isLoading: false
    })
  )
)

export function reducer(state: UserProfileStateInterface, action: Action) {
  return userProfileReducer(state, action)
}
