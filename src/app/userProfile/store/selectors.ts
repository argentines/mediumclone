import {createFeatureSelector, createSelector} from '@ngrx/store'

import {AppStateInterface} from 'src/app/shared/types/appState.interface'
import {UserProfileStateInterface} from 'src/app/userProfile/types/UserProfileState.Interface'

export const userProfileFeatureSelector = createFeatureSelector<AppStateInterface, UserProfileStateInterface>('userProfile')

export const isLoadingSelector = createSelector(
  userProfileFeatureSelector,
  (userProfile: UserProfileStateInterface) => userProfile.isLoading)

export const errorsSelector = createSelector(
  userProfileFeatureSelector,
  (userProfile: UserProfileStateInterface) => userProfile.error)

export const userProfileSelector = createSelector(
  userProfileFeatureSelector,
(userProfile: UserProfileStateInterface) => userProfile.data)



