import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'
import {UserProfileComponent} from 'src/app/userProfile/components/user-profile.component'

const routes: Routes = [
  {path:'profiles/:slug', component: UserProfileComponent},
  {path:'profiles/:slug/favorites', component: UserProfileComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class UserProfileRoutingModule {}
