import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {EffectsModule} from '@ngrx/effects'
import {StoreModule} from '@ngrx/store'

import {UserProfileComponent} from 'src/app/userProfile/components/user-profile.component'
import {UserProfileRoutingModule} from 'src/app/userProfile/user-profile-routing.module'
import {FeedModule} from 'src/app/shared/modules/feed/feed.module'

import {UserProfileService} from 'src/app/userProfile/services/user-profile.service'

import {GetUserProfileEffect} from 'src/app/userProfile/store/effects/getUserProfile.effect'
import {reducer} from 'src/app/userProfile/store/reducers'

@NgModule({
  declarations: [UserProfileComponent],
  imports: [CommonModule,
            UserProfileRoutingModule,
            FeedModule,
            EffectsModule.forFeature([GetUserProfileEffect]),
            StoreModule.forFeature('userProfile', reducer)],
  providers: [UserProfileService]
})
export class UserProfileModule {}
