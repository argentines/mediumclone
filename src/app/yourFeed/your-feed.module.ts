import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {YourFeedComponent} from 'src/app/yourFeed/components/your-feed/your-feed.component'

import {YourFeedRoutingModule} from 'src/app/yourFeed/your-feed-routing.module'
import {FeedModule} from 'src/app/shared/modules/feed/feed.module'
import {PopularTagsModule} from 'src/app/shared/modules/popularTags/popular-tags.module'
import {FeedTogglerModule} from 'src/app/shared/modules/feedToggler/feed-toggler.module'
import {BannerModule} from 'src/app/shared/modules/banner/banner.module'

@NgModule({
  imports: [CommonModule,
            YourFeedRoutingModule,
            FeedTogglerModule,
            FeedModule,
            PopularTagsModule,
            BannerModule],
  declarations: [YourFeedComponent]
})
export class YourFeedModule {}
