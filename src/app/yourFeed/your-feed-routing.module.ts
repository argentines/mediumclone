import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'
import {YourFeedComponent} from 'src/app/yourFeed/components/your-feed/your-feed.component'

const routes: Routes = [{
  path:'feed', component: YourFeedComponent
}]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class YourFeedRoutingModule {}
