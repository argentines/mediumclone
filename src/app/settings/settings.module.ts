import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {StoreModule} from '@ngrx/store'
import {ReactiveFormsModule} from '@angular/forms'

import {SettingsRoutingModule} from 'src/app/settings/settings-routing.module'
import {BackendErrorsMessageModule} from 'src/app/shared/modules/backendErrorMessages/backendErrorsMessage.module'

import {SettingsComponent} from 'src/app/settings/components/settings/settings.component'
import {reducers} from 'src/app/settings/store/reducers'

@NgModule({
  imports: [CommonModule,
            SettingsRoutingModule,
            BackendErrorsMessageModule,
            ReactiveFormsModule,
            StoreModule.forFeature('settings', reducers)],
  declarations: [SettingsComponent]
})

export class SettingsModule{}
