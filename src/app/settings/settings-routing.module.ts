import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'

import {SettingsComponent} from 'src/app/settings/components/settings/settings.component'

const routes: Routes = [{
  path:'settings', component: SettingsComponent
}]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {}
