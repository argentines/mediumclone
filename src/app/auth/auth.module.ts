import {NgModule} from '@angular/core'

import {CommonModule} from '@angular/common'
import {StoreModule} from '@ngrx/store'
import {ReactiveFormsModule} from '@angular/forms'
import {AuthRoutingModule} from 'src/app/auth/auth-routing.module'
import {EffectsModule} from '@ngrx/effects'
import {BackendErrorsMessageModule} from 'src/app/shared/modules/backendErrorMessages/backendErrorsMessage.module'

import {AuthService} from 'src/app/auth/services/auth.service'
import {PersistenceService} from 'src/app/shared/services/persistence.service'

import {RegisterComponent} from 'src/app/auth/components/register/register.component'
import {LoginComponent} from 'src/app/auth/components/login/login.component'
import {reducers} from 'src/app/auth/store/reducers'

import {LoginEffect} from 'src/app/auth/store/effects/login.effect'
import {GetCurrentUserEffect} from 'src/app/auth/store/effects/getCurrentUser.effect'
import {UpdateCurrentUserEffect} from 'src/app/auth/store/effects/updateCurrentUser.effect'
import {LogoutEffect} from 'src/app/auth/store/effects/logout.effect'
import {RegisterEffect} from 'src/app/auth/store/effects/register.effect'


@NgModule({
  declarations: [
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    StoreModule.forFeature('auth', reducers),
    EffectsModule.forFeature([LoginEffect,
                                         GetCurrentUserEffect,
                                         UpdateCurrentUserEffect,
                                         LogoutEffect,
                                         RegisterEffect]),
    BackendErrorsMessageModule
  ],
  providers: [AuthService, PersistenceService]
})
export class AuthModule { }
