import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'

import {RegisterRequestInterface} from 'src/app/auth/types/registerRequest.interface'
import {CurrentUserInterface} from 'src/app/shared/types/currentUser.interface'
import {AuthResponseInterface} from 'src/app/auth/types/authResponse.interface'
import {LoginRequestInterface} from 'src/app/auth/types/loginRequest.interface'

import {environment} from 'src/environments/environment'

import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'
import {CurrentUserInputInterface} from 'src/app/shared/services/current-user-input.interface'

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {}

  register(data: RegisterRequestInterface): Observable<CurrentUserInterface> {
    const url = `${environment.apiUrl}/users`

    return this.http.post<AuthResponseInterface>(url, data)
                    .pipe(map(this.getUser))
  }

  login(data: LoginRequestInterface): Observable<CurrentUserInterface> {
    const url = `${environment.apiUrl}/users/login`
    return this.http.post<AuthResponseInterface>(url, data)
                    .pipe(map(this.getUser))
  }

  getUser(response: AuthResponseInterface): CurrentUserInterface {
    return response.user
  }

  getCurrentUser(): Observable<CurrentUserInterface> {
    const url = `${environment.apiUrl}/user`
    return this.http.get(url).pipe(map(this.getUser))
  }

  updateCurrentUser(currentUserInput: CurrentUserInputInterface): Observable<CurrentUserInterface> {
    const url = `${environment.apiUrl}/user`
    return this.http.put(url, currentUserInput).pipe(map(this.getUser));
  }
}
