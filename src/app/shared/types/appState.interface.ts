import {AuthStateInterface} from 'src/app/auth/types/authState.interface'
import {FeedStateInterface} from 'src/app/shared/modules/feed/types/feed-state-interface'
import {PopularTagsStateInterface} from 'src/app/shared/modules/popularTags/types/popular-tags-state.interface'
import {ArticleStateInterface} from 'src/app/article/types/article-state-interface'
import {CreateArticleStateInterface} from 'src/app/createArticle/types/createArticleState.interface'
import {EditArticleStateInterface} from 'src/app/editArticle/types/editArticleState.interface'
import {SettingsStateInterface} from 'src/app/settings/types/settingsState.interface'
import {UserProfileStateInterface} from 'src/app/userProfile/types/UserProfileState.Interface'

export interface AppStateInterface {
  auth: AuthStateInterface
  feed: FeedStateInterface
  popularTags: PopularTagsStateInterface
  article: ArticleStateInterface
  createArticle: CreateArticleStateInterface,
  editArticle: EditArticleStateInterface,
  settings: SettingsStateInterface,
  userProfile: UserProfileStateInterface
}
