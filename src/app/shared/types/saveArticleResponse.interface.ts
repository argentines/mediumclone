import {ArticleInputInterface} from 'src/app/shared/types/articleInput.interface'

export interface SaveArticleResponseInterface {
  article: ArticleInputInterface
}
