import { Injectable } from '@angular/core'

@Injectable()
export class PersistenceService {

  set(key: string, data: any) {
    try {
      localStorage.setItem(key, JSON.stringify(data))
    }catch(e) {
      console.log('Error set data to localStorage, in token. ', e)
    }
  }

  get(key: string) {
    try {
      return JSON.parse(localStorage.getItem(key))
    }catch(e) {
      console.log('Error get data from localStorage in token. ', e)
      return null;
    }
  }
}
