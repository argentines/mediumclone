import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {StoreModule} from '@ngrx/store'
import {EffectsModule} from '@ngrx/effects'
import {RouterModule} from '@angular/router'

import {reducers} from 'src/app/shared/modules/popularTags/store/reducers'

import {LoadingModule} from 'src/app/shared/modules/loading/loading.module'
import {ErrorMessageModule} from 'src/app/shared/modules/errorMessage/error-message.module'

import {PopularTagsServices} from 'src/app/shared/modules/popularTags/services/popular-tags.services'
import {GetPopularTagsEffect} from 'src/app/shared/modules/popularTags/store/effects/getPopularTags.effect'

import {PopularTagsComponent} from 'src/app/shared/modules/popularTags/components/popular-tags/popular-tags.component'

@NgModule({
  imports: [CommonModule,
    StoreModule.forFeature('popularTags', reducers),
    EffectsModule.forFeature([GetPopularTagsEffect]),
    RouterModule,
    LoadingModule,
    ErrorMessageModule],
  declarations: [PopularTagsComponent],
  exports: [PopularTagsComponent],
  providers: [PopularTagsServices]
})

export class PopularTagsModule {}
