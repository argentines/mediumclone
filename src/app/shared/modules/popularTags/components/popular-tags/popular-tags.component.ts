import { Component, OnInit } from '@angular/core'
import {select, Store} from '@ngrx/store'
import {Observable} from 'rxjs'

import {getPopularTagsAction} from 'src/app/shared/modules/popularTags/store/actions/getPopularTags.action'
import {errorSelector,
        isLoadingSelector,
        popularTagsSelector} from 'src/app/shared/modules/popularTags/store/selectors'

import {PopularTagType} from 'src/app/shared/types/popularTagType.type'



@Component({
  selector: 'mc-popular-tags',
  templateUrl: './popular-tags.component.html',
  styleUrls: ['./popular-tags.component.scss']
})
export class PopularTagsComponent implements OnInit {

  popularTags$: Observable<PopularTagType[] | null>
  isLoading$: Observable<boolean>
  error$: Observable<string | null>

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.initializeValues()
    this.fetchData()
  }

  private initializeValues() {
    this.popularTags$ = this.store.pipe(select(popularTagsSelector))
    this.isLoading$ = this.store.pipe(select(isLoadingSelector))
    this.error$ = this.store.pipe(select(errorSelector))
  }

  private fetchData() {
    this.store.dispatch(getPopularTagsAction())
  }
}
