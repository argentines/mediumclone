import {PopularTagType} from 'src/app/shared/types/popularTagType.type'

export interface GetPopularTagsResponseInterface {
  tags: PopularTagType[]
}
