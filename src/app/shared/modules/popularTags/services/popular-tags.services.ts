import {Injectable} from '@angular/core'
import {environment} from 'src/environments/environment'
import {HttpClient} from '@angular/common/http'

import {PopularTagType} from 'src/app/shared/types/popularTagType.type'
import {GetPopularTagsResponseInterface} from 'src/app/shared/modules/popularTags/types/get-popular-tags-response.Interface'

import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'

@Injectable()
export class PopularTagsServices {

  constructor(private http: HttpClient) {}

  getPopularTags(): Observable<PopularTagType[]> {
    const url = `${environment.apiUrl}/tags`
    return this.http.get(url)
                    .pipe(
                          map((response: GetPopularTagsResponseInterface) => {
                              return response.tags
                          })
                    )
  }
}
