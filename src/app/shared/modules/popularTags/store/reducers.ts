import {PopularTagsStateInterface} from 'src/app/shared/modules/popularTags/types/popular-tags-state.interface'
import {Action, createReducer, on} from '@ngrx/store'
import {getPopularTagsAction,
        getPopularTagsFailureAction,
        getPopularTagsSuccessAction} from 'src/app/shared/modules/popularTags/store/actions/getPopularTags.action'

const initialState: PopularTagsStateInterface = {
  data: null,
  isLoading: false,
  error: null
}

const popularTagsReducer = createReducer(
  initialState,
  on(getPopularTagsAction,
(state) => ({
          ...state,
          isLoading: true
       })
  ),
  on(getPopularTagsSuccessAction,
    (state, action) => ({
        ...state,
        isLoading: false,
        data: action.popularTags
    })
  ),
  on(getPopularTagsFailureAction,
    (state) => ({
        ...state,
        isLoading: false
    })
  )
)

export function reducers(state: PopularTagsStateInterface, action: Action) {
  return popularTagsReducer(state, action)
}


