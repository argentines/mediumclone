import {Injectable} from '@angular/core'
import {Actions, createEffect, ofType} from '@ngrx/effects'
import {catchError, map, switchMap} from 'rxjs/operators'
import {of} from 'rxjs'

import {getPopularTagsAction,
        getPopularTagsFailureAction,
        getPopularTagsSuccessAction} from 'src/app/shared/modules/popularTags/store/actions/getPopularTags.action'

import {PopularTagsServices} from 'src/app/shared/modules/popularTags/services/popular-tags.services'

import {PopularTagType} from 'src/app/shared/types/popularTagType.type'

@Injectable()
export class GetPopularTagsEffect {

  getPopularTags$ = createEffect(() => {
      return this.actions$.pipe(
          ofType(getPopularTagsAction),
          switchMap(() => {
            return this.popularTagsService.getPopularTags().pipe(
              map((popularTags: PopularTagType[]) => {
                return getPopularTagsSuccessAction({popularTags})
              }),
              catchError(() => {
                return of(getPopularTagsFailureAction());
              })
            );
          })
      );
  });

  constructor(private actions$: Actions,
              private popularTagsService: PopularTagsServices) {}
}
