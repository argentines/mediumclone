import {NgModule} from '@angular/core'
import {RouterModule} from '@angular/router'
import {CommonModule} from '@angular/common'

import {FeedTogglerComponent} from 'src/app/shared/modules/feedToggler/components/feed-toggler/feed-toggler.component'


@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [FeedTogglerComponent],
  exports: [FeedTogglerComponent]
})

export class FeedTogglerModule {}
