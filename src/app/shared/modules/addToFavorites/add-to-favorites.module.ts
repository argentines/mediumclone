import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {EffectsModule} from '@ngrx/effects'

import {AddToFavoritesComponent} from 'src/app/shared/modules/addToFavorites/components/add-to-favorites/add-to-favorites.component'
import {AddToFavoritesService} from 'src/app/shared/modules/addToFavorites/services/add-to-favorites.service'

import {AddToFavoritesEffect} from 'src/app/shared/modules/addToFavorites/store/effects/add-to-favorites.effect'

@NgModule({
  declarations: [AddToFavoritesComponent],
  imports: [CommonModule, EffectsModule.forFeature([AddToFavoritesEffect])],
  exports: [AddToFavoritesComponent],
  providers: [AddToFavoritesService]
})

export class AddToFavoritesModule {}
