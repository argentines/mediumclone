import {Injectable} from '@angular/core'
import {Actions, createEffect, ofType} from '@ngrx/effects'
import {catchError, map, switchMap} from 'rxjs/operators'
import {of} from 'rxjs'

import {addToFavoritesAction, addToFavoritesFailureAction, addToFavoritesSuccessAction} from 'src/app/shared/modules/addToFavorites/store/actions/addToFavorite.action'
import {AddToFavoritesService} from 'src/app/shared/modules/addToFavorites/services/add-to-favorites.service'
import {ArticleInterface} from 'src/app/shared/types/article.interface'

@Injectable()
export class AddToFavoritesEffect {

  addFavorites$ = createEffect(() => {
      return this.actions$.pipe(
          ofType(addToFavoritesAction),
          switchMap(({isFavorited, slug}) => {
            const article$ = isFavorited
                             ? this.addToFavoritesService.removeFromFavorites(slug)
                             : this.addToFavoritesService.addToFavorites(slug)
            return article$.pipe(
              map((article: ArticleInterface) => {
                return addToFavoritesSuccessAction({article})
              }),
              catchError(() => {
                return of(addToFavoritesFailureAction())
              })
            );
          })
      );
  });

  constructor(private actions$: Actions,
              private addToFavoritesService: AddToFavoritesService) {}
}
