import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {ReactiveFormsModule} from '@angular/forms'
import {BackendErrorsMessageModule} from 'src/app/shared/modules/backendErrorMessages/backendErrorsMessage.module'

import {ArticleFormComponent} from 'src/app/shared/modules/articleForm/components/article-form/article-form.component'

@NgModule({
  imports: [CommonModule,
            ReactiveFormsModule,
            BackendErrorsMessageModule],
  exports: [ArticleFormComponent],
  declarations: [ArticleFormComponent]
})

export class ArticleFormModule {}
