import {Component, Input, OnInit} from '@angular/core'
import {BackendErrorsInterface} from 'src/app/shared/types/backendErrors.interface'

@Component({
  selector: 'mc-backand-errors-messages',
  templateUrl: './backandErrorsMessages.component.html',
  styleUrls: ['./backandErrorsMessages.component.scss']
})
export class BackandErrorsMessagesComponent implements OnInit {

  @Input('backandErrors') backendErrorsProps: BackendErrorsInterface

  errorMessages: string[]

  ngOnInit(): void {
    this.errorMessages = Object.keys(this.backendErrorsProps).map(
        (name: string) => {
          const message = this.backendErrorsProps[name].join(', ')
          return `${name} ${message}`
        }
    );
  }

}
