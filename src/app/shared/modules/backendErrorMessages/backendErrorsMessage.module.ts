import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {BackandErrorsMessagesComponent} from 'src/app/shared/modules/backendErrorMessages/components/backandErrorsMessages/backandErrorsMessages.component'

@NgModule({
  imports: [CommonModule],
  declarations: [BackandErrorsMessagesComponent],
  exports: [BackandErrorsMessagesComponent]
})
export class BackendErrorsMessageModule {}
