import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {TopBarComponent } from 'src/app/shared/modules/topBar/components/topBar/top-bar.component'
import {RouterModule} from '@angular/router'

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [TopBarComponent],
  declarations: [TopBarComponent]
})
export class TopBarModule { }
