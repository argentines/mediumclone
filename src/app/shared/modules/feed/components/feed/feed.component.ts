import {Component, Input, OnDestroy, OnInit, SimpleChanges} from '@angular/core'
import {select, Store} from '@ngrx/store'
import {Observable, Subscription} from 'rxjs'
import {environment} from 'src/environments/environment'
import {ActivatedRoute, Params, Router} from '@angular/router'

import {getFeedAction} from 'src/app/shared/modules/feed/store/actions/getFeed.action'
import {errorsSelector, feedSelector, isLoadingSelector} from 'src/app/shared/modules/feed/store/selectors'

import {GetFeedResponseInterface} from 'src/app/shared/modules/feed/types/get-feed-response.interface'
import {parseUrl, stringify} from 'query-string'

@Component({
  selector: 'mc-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit, OnDestroy {

  @Input('apiUrl') apiUrlProps: string

  isLoading$: Observable<boolean>
  error$: Observable<string | null>
  feed$: Observable<GetFeedResponseInterface | null>

  baseUrl: string
  limit = environment.limit
  currentPage: number
  queryParamSubscribe: Subscription


  constructor(private store: Store, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.initializeValues()
    this.initializeListeners()
  }

  ngOnDestroy() {
    if(this.queryParamSubscribe) {
      this.queryParamSubscribe.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const isApiUrlChanged = !changes.apiUrlProps.firstChange
                            && changes.apiUrlProps.currentValue !== changes.apiUrlProps.previousValue
    if(isApiUrlChanged) {
      this.fetchFeed()
    }
  }

  initializeValues(): void {
    this.isLoading$ = this.store.pipe(select(isLoadingSelector))
    this.error$ = this.store.pipe(select(errorsSelector))
    this.feed$ = this.store.pipe(select(feedSelector))
    this.baseUrl = this.router.url.split('?')[0]
  }

  fetchFeed(): void {
    const offset = this.currentPage * this.limit - this.limit
    const parsedUrl = parseUrl(this.apiUrlProps)
    const stringifiedParams = stringify({
      limit: this.limit,
      offset,
      ...parsedUrl.query
    })
    const apiUrlWithParams = `${parsedUrl.url}?${stringifiedParams}`
    this.store.dispatch(getFeedAction({url: apiUrlWithParams}))
  }

  initializeListeners(): void {
    this.queryParamSubscribe = this.route.queryParams.subscribe((params: Params) => {
        this.currentPage = Number(params.page || '1')
        this.fetchFeed()
    })
  }
}
