import {NgModule} from '@angular/core'
import {RouterModule} from '@angular/router'
import {CommonModule} from '@angular/common'
import {EffectsModule} from '@ngrx/effects'
import {StoreModule} from '@ngrx/store'
import {GetFeedEffect} from 'src/app/shared/modules/feed/store/effects/getFeed.effect'
import {ErrorMessageModule} from 'src/app/shared/modules/errorMessage/error-message.module'
import {LoadingModule} from 'src/app/shared/modules/loading/loading.module'
import {PaginationModule} from 'src/app/shared/modules/pagination/pagination.module'
import {TageListModule} from 'src/app/shared/modules/tageList/tage-list.module'

import {reducers} from 'src/app/shared/modules/feed/store/reducers'
import {FeedService} from 'src/app/shared/modules/feed/services/feed.service'

import {FeedComponent} from 'src/app/shared/modules/feed/components/feed/feed.component'
import {AddToFavoritesModule} from 'src/app/shared/modules/addToFavorites/add-to-favorites.module'

@NgModule({
    imports: [CommonModule,
              EffectsModule.forFeature([GetFeedEffect]),
              StoreModule.forFeature('feed', reducers),
              RouterModule,
              ErrorMessageModule,
              LoadingModule,
              PaginationModule,
              AddToFavoritesModule,
              TageListModule],
  declarations: [FeedComponent],
  exports: [FeedComponent],
  providers: [FeedService]
})
export class FeedModule {}
