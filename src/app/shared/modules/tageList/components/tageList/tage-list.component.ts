import {Component, Input, OnInit} from '@angular/core';
import {PopularTagType} from 'src/app/shared/types/popularTagType.type'

@Component({
  selector: 'mc-tage-list',
  templateUrl: './tage-list.component.html',
  styleUrls: ['./tage-list.component.scss']
})
export class TageListComponent {

  @Input('tags') tagsProps: PopularTagType[]

}
