import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {TageListComponent} from 'src/app/shared/modules/tageList/components/tageList/tage-list.component'

@NgModule({
  imports: [CommonModule],
  declarations: [TageListComponent],
  exports: [TageListComponent]
})
export class TageListModule {}
