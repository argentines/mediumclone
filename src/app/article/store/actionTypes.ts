export enum ActionTypes {
  GET_ARTICLE = '[Article] Article feed',
  GET_ARTICLE_SUCCESS = '[Article] Get Article success',
  GET_ARTICLE_FAILURE = '[Article] Get Article failure',

  DELETE_ARTICLE = '[Article] Delete feed',
  DELETE_ARTICLE_SUCCESS = '[Article] Delete Article success',
  DELETE_ARTICLE_FAILURE = '[Article] Delete Article failure',
}
