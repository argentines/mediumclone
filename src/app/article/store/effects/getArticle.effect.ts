import {Injectable} from '@angular/core'
import {Actions, createEffect, ofType} from '@ngrx/effects'
import {catchError, map, switchMap} from 'rxjs/operators'
import {of} from 'rxjs'

import {ArticleService as SharedArticleService} from 'src/app/shared/services/article.service'
import {getArticleAction, getArticleFailureAction, getArticleSuccessAction} from 'src/app/article/store/actions/article.action'
import {ArticleInterface} from 'src/app/shared/types/article.interface'

@Injectable()
export class GetArticleEffect {

  getArticle$ = createEffect(() => {
      return this.actions$.pipe(
          ofType(getArticleAction),
          switchMap(({slug}) => {
            return this.sharedArticleService.getArticle(slug).pipe(
              map((article: ArticleInterface) => {
                return getArticleSuccessAction({article});
              }),
              catchError(() => {
                return of(getArticleFailureAction());
              })
            );
          })
      );
  });

  constructor(private actions$: Actions,
              private sharedArticleService: SharedArticleService) {}
}
