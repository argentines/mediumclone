import {NgModule} from '@angular/core'
import {RouterModule} from '@angular/router'
import {CommonModule} from '@angular/common'
import {EffectsModule} from '@ngrx/effects'
import {StoreModule} from '@ngrx/store'
import {TageListModule} from 'src/app/shared/modules/tageList/tage-list.module'
import {ArticleService} from 'src/app/article/services/article.service'

import {ErrorMessageModule} from 'src/app/shared/modules/errorMessage/error-message.module'
import {LoadingModule} from 'src/app/shared/modules/loading/loading.module'
import {ArticleRoutingModule} from 'src/app/article/article-routing.module'

import {reducers} from 'src/app/article/store/reducers'

import {ArticleComponent} from 'src/app/article/components/article/article.component'
import {ArticleService as SharedArticleService} from 'src/app/shared/services/article.service'
import {GetArticleEffect} from 'src/app/article/store/effects/getArticle.effect'
import {DeleteArticleEffect} from 'src/app/article/store/effects/deleteArticle.effect'


@NgModule({
    imports: [CommonModule,
              EffectsModule.forFeature([GetArticleEffect, DeleteArticleEffect]),
              StoreModule.forFeature('article', reducers),
              RouterModule,
              ErrorMessageModule,
              LoadingModule,
              TageListModule,
              ArticleRoutingModule],
  declarations: [ArticleComponent],
  exports: [],
  providers: [SharedArticleService, ArticleService]
})
export class ArticleModule {}
