import { Component, OnInit } from '@angular/core'
import {select, Store} from '@ngrx/store'
import {Observable} from 'rxjs'
import {ActivatedRoute} from '@angular/router'

import {ArticleInputInterface} from 'src/app/shared/types/articleInput.interface'
import {BackendErrorsInterface} from 'src/app/shared/types/backendErrors.interface'
import {filter, map} from 'rxjs/operators'

import {isSubmittingSelector, validationErrorsSelector} from 'src/app/createArticle/store/selectors'
import {updateArticleAction} from 'src/app/editArticle/store/actions/updateArticle.action'
import {getArticleAction} from 'src/app/editArticle/store/actions/getArticle.action'
import {articleSelector} from 'src/app/editArticle/store/selectors'

import {ArticleInterface} from 'src/app/shared/types/article.interface'

@Component({
  selector: 'mc-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.scss']
})
export class EditArticleComponent implements OnInit {

  initialValues$: Observable<ArticleInputInterface>
  isSubmitting$: Observable<boolean>
  isLoading$: Observable<boolean>
  backandErrors$: Observable<BackendErrorsInterface | null>
  slug: string;

  constructor(private store: Store, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.initializeValues()
    this.fetchData()
  }

  initializeValues(): void {
    this.slug = this.route.snapshot.paramMap.get('slug')
    this.isSubmitting$ = this.store.pipe(select(isSubmittingSelector))
    this.backandErrors$ = this.store.pipe(select(validationErrorsSelector))
    this.initialValues$ = this.store.pipe(select(articleSelector),
                                          filter(Boolean),
                                          map((article: ArticleInterface) => {
                                            return {
                                              title: article.title,
                                              description: article.description,
                                              body: article.body,
                                              tagList: article.tagList
                                            }
                                          })) //что бы быть уверенным что не будет null можно применить filter(Boolean)
  }

  fetchData(): void {
    this.store.dispatch(getArticleAction({slug: this.slug}))
  }

  onSubmit(articleInput: ArticleInputInterface): void {
    this.store.dispatch(updateArticleAction({slug: this.slug, articleInput}))
  }
}
