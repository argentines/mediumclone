import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {EffectsModule} from '@ngrx/effects'
import {StoreModule} from '@ngrx/store'
import {reducers} from 'src/app/editArticle/store/reducers'

import {ArticleFormModule} from 'src/app/shared/modules/articleForm/article-form.module'
import {LoadingModule} from 'src/app/shared/modules/loading/loading.module'

import {EditArticleService} from './services/edit-article.service'
import {ArticleService as SharedArticleService} from 'src/app/shared/services/article.service'

import {EditArticleComponent} from 'src/app/editArticle/components/edit-article/edit-article.component'
import {GetArticleEffect} from 'src/app/editArticle/store/effects/getArticle.effect'
import {UpdateArticleEffect} from 'src/app/editArticle/store/effects/updateArticle.effect'
import {EditArticleRoutingModule} from 'src/app/editArticle/edit-article-routing.module'

@NgModule({
  imports: [CommonModule,
    EditArticleRoutingModule,
    ArticleFormModule,
    EffectsModule.forFeature([UpdateArticleEffect, GetArticleEffect]),
    StoreModule.forFeature('editArticle', reducers), LoadingModule],
  providers: [EditArticleService, SharedArticleService],
  declarations: [EditArticleComponent]
})

export class EditArticleModule {}
