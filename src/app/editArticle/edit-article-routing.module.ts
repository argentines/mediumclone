import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'

import {EditArticleComponent} from 'src/app/editArticle/components/edit-article/edit-article.component'

const routes: Routes = [
  {path: 'articles/:slug/edit', component: EditArticleComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditArticleRoutingModule {}
