import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'

import {FeedModule} from 'src/app/shared/modules/feed/feed.module'
import {PopularTagsModule} from 'src/app/shared/modules/popularTags/popular-tags.module'
import {FeedTogglerModule} from 'src/app/shared/modules/feedToggler/feed-toggler.module'
import {BannerModule} from 'src/app/shared/modules/banner/banner.module'
import {TagFeedRoutingModule} from 'src/app/tagFeed/tag-feed-routing.module'

import {TagFeedComponent} from 'src/app/tagFeed/components/tag-feed/tag-feed.component'

@NgModule({
  imports: [CommonModule,
            TagFeedRoutingModule,
            FeedTogglerModule,
            FeedModule,
            PopularTagsModule,
            BannerModule],
  declarations: [TagFeedComponent]
})
export class TagFeedModule {}
