import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'

import {TagFeedComponent} from 'src/app/tagFeed/components/tag-feed/tag-feed.component'

const routes: Routes = [{
  path:'tags/:slug', component: TagFeedComponent
}]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TagFeedRoutingModule {}
